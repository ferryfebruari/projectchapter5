package com.example.projectchapter5.gamewithcpu

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.projectchapter5.R
import kotlin.properties.Delegates

class GameCpuActivity : AppCompatActivity(), CallbackGameCpu {

    private lateinit var batu1: ImageView
    private lateinit var gunting1: ImageView
    private lateinit var kertas1: ImageView
    private lateinit var batu2: ImageView
    private lateinit var gunting2: ImageView
    private lateinit var kertas2: ImageView
    private lateinit var versus: ImageView
    private lateinit var refresh: ImageView
    private lateinit var close: ImageView
    private lateinit var cpuPlayer: TextView
    private lateinit var txtHasil: String
    private lateinit var player: String
    private var clicked by Delegates.notNull<Boolean>()

    private val cpu = mutableListOf("batu", "gunting", "kertas")
    private val controller = ControllerGameCpu(this)

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("ResourceAsColor", "UseCompatLoadingForDrawables")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_cpu)

        initId()

        //ambil data dari HalamanMenu pake parcelable
        val usernameCpu = intent.getParcelableExtra<UsernameCpu>("usernameCpu") as UsernameCpu
        val username = usernameCpu.username
        cpuPlayer.text = username

        batu1.setOnClickListener {
            batu1.setBackgroundResource(R.drawable.select_button)
            player = "batu"
            clicked = false
            toastShow(username, player)
            Log.d(username, "memilih $player")
            controller.algoritma(player, cpu.random(), username)
            dialog()
        }

        gunting1.setOnClickListener {
            gunting1.setBackgroundResource(R.drawable.select_button)
            player = "gunting"
            clicked = false
            toastShow(username, player)
            Log.d(username, "memilih $player")
            controller.algoritma(player, cpu.random(), username)
            dialog()
        }

        kertas1.setOnClickListener {
            kertas1.setBackgroundResource(R.drawable.select_button)
            player = "kertas"
            clicked = false
            toastShow(username, player)
            Log.d(username, "memilih $player")
            controller.algoritma(player, cpu.random(), username)
            dialog()
        }

        refresh.setOnClickListener {
            if (clicked) {
                toastShow(username, "tidak klik apapun.")
            } else reset()
        }

        close.setOnClickListener { finish() }
    }

    override fun sendCpu(computer: String, text: String) {
        initId()
        txtHasil = text

        when (computer) {
            cpu[1] -> {
                gunting2.setBackgroundResource(R.drawable.select_button)
                toastShow("CPU", cpu[1])
                Log.d("CPU", "memilih gunting")
            }
            cpu[0] -> {
                batu2.setBackgroundResource(R.drawable.select_button)
                toastShow("CPU", cpu[0])
                Log.d("CPU", "memilih batu")
            }
            else -> {
                kertas2.setBackgroundResource(R.drawable.select_button)
                toastShow("CPU", cpu[2])
                Log.d("CPU", "memilih kertas")
            }
        }

    }

    override fun sendResult(hasil: Int) {
        versus = findViewById(R.id.ivVersus)
        versus.setImageResource(hasil)
    }


    //FUNCTION - FUNCTION
    //function isinya inisialisasi dari ID
    private fun initId() {
        batu1 = findViewById(R.id.ivBatu1)
        kertas1 = findViewById(R.id.ivKertas1)
        gunting1 = findViewById(R.id.ivGunting1)
        gunting2 = findViewById(R.id.ivGunting2)
        batu2 = findViewById(R.id.ivBatu2)
        kertas2 = findViewById(R.id.ivKertas2)
        refresh = findViewById(R.id.ivRefresh)
        close = findViewById(R.id.ivClose)
        cpuPlayer = findViewById(R.id.tvPLayer1)
    }

    //function toast
    private fun toastShow(username: String, player: String) {
        val layout = layoutInflater.inflate(R.layout.custom_toast, findViewById(R.id.id_custom_toast))
        val textToast = layout.findViewById<TextView>(R.id.tvToast)
        textToast.text = "$username memilih $player"
        with(Toast(applicationContext)) {
            setGravity(Gravity.BOTTOM, 0, 0)
            duration = Toast.LENGTH_SHORT
            view = layout
            show()
        }
    }

    //function reset
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun reset() {
        batu1.background = getDrawable(android.R.color.transparent)
        gunting1.background = getDrawable(android.R.color.transparent)
        kertas1.background = getDrawable(android.R.color.transparent)
        batu2.background = getDrawable(android.R.color.transparent)
        gunting2.background = getDrawable(android.R.color.transparent)
        kertas2.background = getDrawable(android.R.color.transparent)
        versus.setImageResource(R.drawable.versus)
        clicked = true
    }

    //function dialog
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun dialog() {


        val view = LayoutInflater.from(this).inflate(R.layout.dialog_player, null, false)
        val dialogBuilder = AlertDialog.Builder(this)
        val mainLagi = view.findViewById<Button>(R.id.btnMainLagi)
        val kembali = view.findViewById<Button>(R.id.btnKembali)
        val tvHasil = view.findViewById<TextView>(R.id.tvUsername)
        tvHasil.text = txtHasil

        dialogBuilder.setView(view)
        dialogBuilder.setCancelable(false)

        val dialog = dialogBuilder.create()
        dialog.show()

        mainLagi.setOnClickListener {
            reset()
            dialog.dismiss()
        }

        kembali.setOnClickListener { finish() }
    }

}