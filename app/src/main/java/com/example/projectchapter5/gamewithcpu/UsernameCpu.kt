package com.example.projectchapter5.gamewithcpu

import android.os.Parcel
import android.os.Parcelable

data class UsernameCpu(val username: String) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readString().toString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(username)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UsernameCpu> {
        override fun createFromParcel(parcel: Parcel): UsernameCpu {
            return UsernameCpu(parcel)
        }

        override fun newArray(size: Int): Array<UsernameCpu?> {
            return arrayOfNulls(size)
        }
    }
}