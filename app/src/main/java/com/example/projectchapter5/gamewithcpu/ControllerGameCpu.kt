package com.example.projectchapter5.gamewithcpu

import com.example.projectchapter5.R

class ControllerGameCpu(private var callBack: CallbackGameCpu) {
    private var hasil = 0
    private var text = ""

    fun algoritma(player: String, computer: String, username: String?) {
        when {
            player == "gunting" && computer == "kertas" || player == "batu" && computer == "gunting" || player == "kertas" && computer == "batu" -> {
                hasil = R.drawable.player_1_menang
                text = "$username\nMENANG!"
            }
            computer == "gunting" && player == "kertas" || computer == "batu" && player == "gunting" || computer == "kertas" && player == "batu" -> {
                hasil = R.drawable.player_2_menang
                text = "CPU\nMENANG!"
            }
            else -> {
                hasil = R.drawable.draw_1
                text = "SERI!"
            }
        }
        callBack.sendCpu(computer, text)
        callBack.sendResult(hasil)

    }
}