package com.example.projectchapter5.gamewithcpu

interface CallbackGameCpu {
    fun sendResult(hasil: Int)
    fun sendCpu(cpu: String, text: String)
}