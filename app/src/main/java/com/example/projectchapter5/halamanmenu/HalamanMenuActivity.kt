package com.example.projectchapter5.halamanmenu

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.projectchapter5.gamewithcpu.GameCpuActivity
import com.example.projectchapter5.gamewithplayer.GamePlayerActivity
import com.example.projectchapter5.R
import com.example.projectchapter5.gamewithcpu.UsernameCpu
import com.example.projectchapter5.gamewithplayer.UsernamePlayer
import com.google.android.material.snackbar.Snackbar
import java.io.Serializable

class HalamanMenuActivity : AppCompatActivity(), Serializable {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_halaman_menu)
        val player = findViewById<TextView>(R.id.tvTextPlayer)
        val cpu = findViewById<TextView>(R.id.tvTextCpu)
        val imgPlayer = findViewById<ImageView>(R.id.ivImgPlayer)
        val imgCpu = findViewById<ImageView>(R.id.ivImgCpu)

        val username = intent.getStringExtra("username")
        player.text = username.plus(" vs Pemain")
        cpu.text = username.plus(" vs CPU")

        val snackBar = Snackbar.make(findViewById(android.R.id.content), "Selamat Datang $username", Snackbar.LENGTH_INDEFINITE)
        snackBar.setAction("Tutup") {
            snackBar.dismiss()
        }
        snackBar.show()

        imgPlayer.setOnClickListener {
//            val fm = supportFragmentManager
//                fm.beginTransaction()
//                    .replace(R.id.viewPager2 ,GamePlayerActivity() ,"Player")
//                    .commit()
            val intent = Intent(this, GamePlayerActivity::class.java)
            val usernamePlayer = UsernamePlayer("$username")
            intent.putExtra("usernamePlayer", usernamePlayer)
            startActivity(intent)

        }

        imgCpu.setOnClickListener {
            val intent = Intent(this, GameCpuActivity::class.java)
            val usernameCpu = UsernameCpu("$username")
            intent.putExtra("usernameCpu", usernameCpu)
            startActivity(intent)
        }
    }
}