package com.example.projectchapter5.gamewithplayer

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.projectchapter5.R
import kotlin.properties.Delegates

class GamePlayerActivity : AppCompatActivity(), CallbackGamePlayer {
    private lateinit var batu1: ImageView
    private lateinit var gunting1: ImageView
    private lateinit var kertas1: ImageView
    private lateinit var batu2: ImageView
    private lateinit var gunting2: ImageView
    private lateinit var kertas2: ImageView
    private lateinit var versus: ImageView
    private lateinit var refresh: ImageView
    private lateinit var close: ImageView
    private lateinit var player: TextView

    private lateinit var txtHasil: String
    private lateinit var player1: String
    private lateinit var player2: String

    private var clicked by Delegates.notNull<Boolean>()
    private var clicked2 by Delegates.notNull<Boolean>()
    private val controller = ControllerGamePlayer(this)

    @SuppressLint("ResourceAsColor", "UseCompatLoadingForDrawables")
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_player)

        initId()
        clicked = true
        clicked2 = true

        //ambil data dari HalamanMenu pake serializable
        val usernamePlayer = intent.getSerializableExtra("usernamePlayer") as UsernamePlayer
        val username = usernamePlayer.username
        player.text = username

        batu1.setOnClickListener {
            if (clicked && clicked2) {
                batu1.setBackgroundResource(R.drawable.select_button)
                player1 = "batu"
                clicked = false
                toastShow(username, player1)
                Log.d(username, "memilih $player1")

            } else toastShow("Player 2 silahkan", "terlebih dahulu.")

        }

        gunting1.setOnClickListener {
            if (clicked && clicked2) {
                gunting1.setBackgroundResource(R.drawable.select_button)
                player1 = "gunting"
                clicked = false
                toastShow(username, player1)
                Log.d(username, "memilih $player1")

            } else toastShow("Player 2 silahkan", "terlebih dahulu.")

        }


            kertas1.setOnClickListener {
                if (clicked && clicked2) {
                    kertas1.setBackgroundResource(R.drawable.select_button)
                    player1 = "kertas"
                    clicked = false
                    toastShow(username, player1)
                    Log.d(username, "memilih $player1")

                } else toastShow("Player 2 silahkan", "terlebih dahulu.")

            }

            batu2.setOnClickListener {
                if (clicked && clicked2) toastShow(username, "terlebih dahulu.")

                 else {
                    batu2.setBackgroundResource(R.drawable.select_button)
                    player2 = "batu"
                    clicked2 = false
                    toastShow("Player2", player2)
                    Log.d("Player 2", "memilih $player2")
                    controller.algoritma(player1, player2, username)
                    dialog()
                }

            }

            gunting2.setOnClickListener {
                if (clicked && clicked2) toastShow(username, "terlebih dahulu.")

                else {
                    gunting2.setBackgroundResource(R.drawable.select_button)
                    player2 = "gunting"
                    clicked2 = false
                    toastShow("Player2", player1)
                    Log.d("Player 2", "memilih $player2")
                    controller.algoritma(player1, player2, username)
                    dialog()
                }

            }

            kertas2.setOnClickListener {
                if (clicked && clicked2) toastShow(username, "terlebih dahulu.")

                else {
                    kertas2.setBackgroundResource(R.drawable.select_button)
                    player2 = "kertas"
                    clicked2 = false
                    toastShow("Player2", player2)
                    Log.d("Player 2", "memilih $player2")
                    controller.algoritma(player1, player2, username)
                    dialog()
                }

            }

            refresh.setOnClickListener {
                if (clicked && clicked2) toastShow("Sudah reset, $username silahkan","pilihannya.")

                 else if (!clicked && clicked2) resetPlayer1()

                 else reset()

            }

            close.setOnClickListener {
                finish()
            }
    }

    override fun sendResult(hasil: Int, text: String) {
        versus = findViewById(R.id.ivVersus)
        versus.setImageResource(hasil)
        txtHasil = text
    }

    //FUNCTION - FUNCTION
    //Function isinya inisiasi dengan ID
    private fun initId() {
        batu1 = findViewById(R.id.ivBatu1)
        kertas1 = findViewById(R.id.ivKertas1)
        gunting1 = findViewById(R.id.ivGunting1)
        gunting2 = findViewById(R.id.ivGunting2)
        batu2 = findViewById(R.id.ivBatu2)
        kertas2 = findViewById(R.id.ivKertas2)
        refresh = findViewById(R.id.ivRefresh)
        close = findViewById(R.id.ivClose)
        player = findViewById(R.id.tvPLayer1)
    }

    //Function untuk custom toast
    private fun toastShow(username: String, player: String) {
        val layout = layoutInflater.inflate(R.layout.custom_toast, findViewById(R.id.id_custom_toast))
        val textToast = layout.findViewById<TextView>(R.id.tvToast)
        textToast.text = "$username memilih $player"

        with(Toast(applicationContext)) {
            setGravity(Gravity.BOTTOM, 0, 0)
            duration = Toast.LENGTH_SHORT
            view = layout
            show()
        }
    }

    //Function untuk reset
    @SuppressLint("UseCompatLoadingForDrawables")
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun reset() {
        batu1.background = getDrawable(android.R.color.transparent)
        gunting1.background = getDrawable(android.R.color.transparent)
        kertas1.background = getDrawable(android.R.color.transparent)
        batu2.background = getDrawable(android.R.color.transparent)
        gunting2.background = getDrawable(android.R.color.transparent)
        kertas2.background = getDrawable(android.R.color.transparent)
        versus.setImageResource(R.drawable.versus)
        clicked = true
        clicked2 = true
    }

    //Function untuk reset player 1 saja
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun resetPlayer1() {
        batu1.background = getDrawable(android.R.color.transparent)
        gunting1.background = getDrawable(android.R.color.transparent)
        kertas1.background = getDrawable(android.R.color.transparent)
        clicked = true
    }

    //Function untuk dialog
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun dialog() {
        val view = LayoutInflater.from(this).inflate(R.layout.dialog_player, null, false)
        val dialogBuilder = AlertDialog.Builder(this)
        val mainLagi = view.findViewById<Button>(R.id.btnMainLagi)
        val kembali = view.findViewById<Button>(R.id.btnKembali)
        val tvHasil = view.findViewById<TextView>(R.id.tvUsername)

        tvHasil.text = txtHasil
        dialogBuilder.setView(view)
        dialogBuilder.setCancelable(false)

        val dialog = dialogBuilder.create()
        dialog.show()

        mainLagi.setOnClickListener {
            reset()
            dialog.dismiss()
        }

        kembali.setOnClickListener {
            finish()
        }
    }
}