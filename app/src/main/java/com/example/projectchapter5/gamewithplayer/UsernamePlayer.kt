package com.example.projectchapter5.gamewithplayer

import java.io.Serializable

data class UsernamePlayer(val username: String) : Serializable