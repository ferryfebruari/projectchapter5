package com.example.projectchapter5.gamewithplayer

import com.example.projectchapter5.R

class ControllerGamePlayer(private var callBack: CallbackGamePlayer) {
        private var hasil = 0
        private var text = ""

        fun algoritma(player1: String, player2: String, username: String?){
            when{
                player1 == "gunting" && player2 == "kertas" || player1 == "batu" && player2 == "gunting" || player1 == "kertas" && player2 == "batu" -> {
                    hasil = R.drawable.player_1_menang
                    text = "$username\nMENANG!"
                }
                player2 == "gunting" && player1 == "kertas" || player2 == "batu" && player1 == "gunting" || player2 == "kertas" && player1 == "batu" -> {
                    hasil = R.drawable.player_2_menang
                    text = "Player 2\nMENANG!"
                }
                else -> {
                    hasil = R.drawable.draw_1
                    text = "SERI!"
                }
            }
            callBack.sendResult(hasil,text)
        }
    }