package com.example.projectchapter5.landingpage

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.example.projectchapter5.halamanmenu.HalamanMenuActivity
import com.example.projectchapter5.R

class LandingPage3Fragment : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_landing_page3, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val play = view.findViewById<ImageView>(R.id.ivPlay)
        val etUsername = view.findViewById<EditText>(R.id.etUsername)

        etUsername.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable?) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.isNotEmpty() == true) {
                    play.visibility = View.VISIBLE
                } else {
                    play.visibility = View.GONE
                }
            }
        })

        play.setOnClickListener {
            val username = etUsername.text.toString()
            val intent = Intent(activity, HalamanMenuActivity::class.java)
            intent.putExtra("username", username)
            activity!!.startActivity(intent)
            activity!!.finish()
        }

    }


}