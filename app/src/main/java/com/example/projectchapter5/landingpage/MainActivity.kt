package com.example.projectchapter5.landingpage

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.example.projectchapter5.*
import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator

class MainActivity : AppCompatActivity() {

    private lateinit var viewPager: ViewPager2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val dataFragment = mutableListOf(LandingPage1Fragment(), LandingPage2Fragment(), LandingPage3Fragment())
        val adapter = Adapter(this)
        adapter.setData(dataFragment)

        viewPager = findViewById(R.id.viewPager2)
        val springDotsIndicator = findViewById<SpringDotsIndicator>(R.id.spring_dots_indicator)
        viewPager.adapter = adapter
        springDotsIndicator.setViewPager2(viewPager)
        viewPager.currentItem = 0
    }


}